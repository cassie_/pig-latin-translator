# frozen_string_literal: true

require 'minitest/autorun'
require_relative 'translator'

# Testing pig latin translations
class TranslatorTest < Minitest::Test
  def test_word_starting_with_vowel_on_chain_strategy
    assert_equal 'appleway', Translator.new('apple').translate('chain')
  end

  def test_word_starting_with_consonant_on_chain_strategy
    assert_equal 'igPay', Translator.new('Pig').translate('chain')
  end

  def test_word_starting_with_qu_on_chain_strategy
    assert_equal 'ayquay', Translator.new('quay').translate('chain')
  end

  def test_sentence_on_chain_strategy
    assert_equal 'igPay atinlay anslatortray',
                 Translator.new('Pig latin translator').translate('chain')
  end
end
