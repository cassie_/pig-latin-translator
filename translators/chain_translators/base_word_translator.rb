# frozen_string_literal: true

# Interface for translator chain components
class BaseWordTranslator
  attr_reader :successor

  def initialize(successor = nil)
    @successor = successor
  end

  def call(word)
    return successor.call(word) unless can_translate?(word)

    translate(word)
  end

  private

  def can_translate?(_)
    false
  end

  def translate(_)
    raise NotImplementedError, 'Each handler should respond to can_translate? and translate'
  end
end
