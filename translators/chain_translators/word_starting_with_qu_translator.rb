# frozen_string_literal: true

# If a word starts with qu we translate it here
class WordStartingWithQuTranslator < BaseWordTranslator
  private

  def translate(word)
    "#{word[2, word.size]}quay"
  end

  def can_translate?(word)
    word.start_with?('qu')
  end
end
