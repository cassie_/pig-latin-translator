# frozen_string_literal: true

# If nothing else can handle the word, this one will
class GenericWordTranslator < BaseWordTranslator
  private

  def translate(word)
    first_consonants_end_index = word.split(/([aeiou].*)/).first.length

    result = word.slice(first_consonants_end_index, word.length)
    "#{result}#{word.slice(0, first_consonants_end_index)}ay"
  end

  def can_translate?(_)
    true
  end
end
