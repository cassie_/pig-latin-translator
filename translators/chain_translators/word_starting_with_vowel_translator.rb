# frozen_string_literal: true

# This handles well, words starting with vowels
class WordStartingWithVowelTranslator < BaseWordTranslator
  private

  def translate(word)
    "#{word}way"
  end

  def can_translate?(word)
    word.start_with?(/a|e|i|o|u/i)
  end
end
