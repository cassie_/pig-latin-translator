# frozen_string_literal: true

require_relative './chain_translators/base_word_translator'
require_relative './chain_translators/generic_word_translator'
require_relative './chain_translators/word_starting_with_qu_translator'
require_relative './chain_translators/word_starting_with_vowel_translator'

# Translates a sentence with a Chain of Responsibility pattern
class ChainTranslator
  def initialize(sentence)
    @sentence = sentence
  end

  def translate
    @sentence.split(' ').map { |w| chain.call(w) }.join(' ')
  end

  private

  def chain
    @chain ||= vowel_translator
  end

  def vowel_translator
    @vowel_translator ||= WordStartingWithVowelTranslator.new(qu_translator)
  end

  def qu_translator
    @qu_translator ||= WordStartingWithQuTranslator.new(generic_translator)
  end

  def generic_translator
    @generic_translator ||= GenericWordTranslator.new
  end
end
