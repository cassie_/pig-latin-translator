# frozen_string_literal: true

require_relative './translators/chain_translator'
# frozen_string_literal: true

# Pig latin translator that implements the Strategy pattern
class Translator
  attr_reader :strategies

  def initialize(sentence)
    @sentence = sentence

    @strategies = {}
    @strategies['chain'] = -> { ChainTranslator.new(@sentence).translate }
  end

  def translate(strategy)
    strategies[strategy].call
  end
end
